<?php
/*
	Template Name: O nas
*/
get_header(); ?>
<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); ?>

    <div class="container onas">
        <div class="row" style="padding-top: 21px;">
            <div class="col-md-3 dem">
                <img src="<?php bloginfo('template_url')?>/images/duze.jpg">
            </div>
            <div class="col-md-9 ple">
                <?php the_content(); ?>
            </div>
        </div>

        <?php if ( have_rows( 'realization_numbers' ) ): ?>
            <div class="row liczby">
                <?php
                while ( have_rows( 'realization_numbers' ) ) : the_row(); ?>
                    <div class="col-md-3 text-center">
                        <p class="cyf" data-counter><?php the_sub_field( 'realization_number' ); ?></p>
                        <p class="cyg"><?php the_sub_field( 'realization_title' ); ?></p>
                    </div>
                    <?php
                endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if ( have_rows( 'about_persons' ) ): ?>
            <?php
            while ( have_rows( 'about_persons' ) ) : the_row(); ?>
                <div class="row zespol">
                    <div class="col-md-3">
                        <img src="<?php the_sub_field( 'about_person_img' ); ?>">
                    </div>
                    <div class="col-md-9 srodek">
                        <div class="col-md-12">
                            <p class="duzy text-left"><?php the_sub_field( 'about_person_title' ); ?></p>
                            <p><?php the_sub_field( 'about_person_desc' ); ?></p>
                        </div>
                    </div>
                </div>
                <?php
            endwhile; ?>
        <?php endif; ?>
    </div>

        <script type="text/javascript">
          ( function( $ ) {
              $(document).ready(function( $ ) {
                $('[data-counter]').counterUp({
                  delay: 10,
                  time: 1000
                });
              });
          } )( jQuery );
        </script>
    <?php endwhile; ?>
<?php endif; ?>
<?php get_template_part( 'call-to-action', 'index' ); ?>
<?php get_footer(); ?>

