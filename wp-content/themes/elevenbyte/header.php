<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58c6608aab48ef44ecd698e6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <nav class="navbar navbar-static-top omenu">
        <div class="container" style="padding-right: 0;">
            <div class="row mango">
                <div class="col-md-6 text-right gora" style="float: right;">
                    <p class="fejs"><?php the_field( 'fb_text', 'option' ); ?></p><a href="<?php the_field( 'fb_link', 'option' ); ?>"><img src="<?php bloginfo('template_url')?>/images/fejs.jpg" class="fico"></a>
                    <ul id="langs" class="list-inline pull-right"><?php pll_the_languages(array('show_flags'=>1,'show_names'=>1)); ?></ul>
                </div>
            </div>
            <div class="row kiwi" style="float: none;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo pll_home_url(pll_current_language()); ?>"><img src="<?php the_field( 'logo', 'option' ); ?>"></a>
                </div>
	            <?php
	            wp_nav_menu( array(
			            'menu'              => 'primary',
			            'theme_location'    => 'primary',
			            'depth'             => 2,
			            'container'         => 'div',
			            'container_class'   => 'navbar-collapse collapse',
			            'container_id'      => 'navbar',
			            'menu_class'        => 'nav navbar-nav navbar-right',
			            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
			            'walker'            => new wp_bootstrap_navwalker())
	            );
	            ?>
            </div>
        </div>
    </nav>

    <div class="site-inner">
        <div class="site-content clearfix">
