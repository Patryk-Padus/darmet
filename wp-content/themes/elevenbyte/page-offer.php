<?php
/*
	Template Name: Oferta
*/
get_header(); ?>

<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <p class="mba duzy"><?php the_title(); ?></p>
	        <?php the_content(); ?>
        </div>
        <div class="col-sm-12 text-center">
            <div class="kryska"></div>
        </div>

	    <?php if ( have_rows( 'offer_points' ) ):
		    while ( have_rows( 'offer_points' ) ) : the_row(); ?>
                <div class="col-sm-12 col-md-6 col-lg-3 text-center janko">
                    <div class="col-sm-12">
                        <img src="<?php the_sub_field( 'offer_point_img' ); ?>">
                    </div>
                    <p class="melon"><?php the_sub_field( 'offer_point_title' ); ?></p>
                </div>
			    <?php
		    endwhile;
	    endif; ?>
    </div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
			<div class="kryska"></div>
		</div>
	</div>
</div>
<div class="container ofer" style="background: none">
    <?php
    $post_objects = get_field('products');
    if( $post_objects ): ?>
        <div class="row">
            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <div class="ofertos col-sm-12 col-md-12 col-lg-12">
                    <div class="col-md-4 text-center">
                        <img src="<?php the_field('product_image'); ?>" />
                    </div>
					<div class="col-md-8">
                    <h2><?php the_title(); ?></h2>
                    <?php the_excerpt(); ?>
					<p class="offer-roz"><?php the_content(); ?></p>
					</div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endif;  ?>
</div>
<?php
$post_objects = get_field('other_products');
if( $post_objects ): ?>
    <div class="container ofer" style="background: none">
        <div class="row">
            <div class="col-sm-12 text-center" style="margin-top: 50px;">
                <p class="duzy"><?php the_field('other_products_title'); ?></p>
            </div>

            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <a href="<?php the_field('product_image'); ?>" class="ofertos col-sm-12 col-md-3 col-lg-2 text-center" data-image>
                    <div class="col-md-12">
                        <img src="<?php the_field('product_image'); ?>">
                    </div>
                    <h2><small><strong><?php the_title(); ?></strong></small></h2>
                </a>
            <?php endforeach; ?>

        </div>
    </div>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif;  ?>

        <div class="container ofer" style="background: none">
            <div class="row">
                <div class="col-sm-12 text-center" style="margin-top: 50px;">
                    <p class="burzy"><?php the_field( 'fast_contact_description', 'option' ); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center nopadding">
                    <p><a class="btn btn-primary btn-huge" href="<?php the_field( 'fast_contact_link', 'option' ); ?>" role="button" style="margin-top: 25px;">Zamów bezpłatną wycenę</a></p>
                </div>
            </div>
        </div>

<?php endwhile; ?>
<?php endif; ?>

<script type="text/javascript">
  ( function( $ ) {
    $('body').on('click','[data-image]', function( e ) {
      e.preventDefault();
      $( '[data-image]' ).swipebox({
        'hideBarsDelay': 0
      });
    } );
  } )( jQuery );
</script>

<?php get_footer(); ?>
