<?php
/*
	Template Name: Strona główna
*/

get_header(); ?>

<?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
    <div class="swiper-container" id="baner">
        <div class="swiper-wrapper">

        <?php if ( have_rows( 'slajder' ) ):
            while ( have_rows( 'slajder' ) ) : the_row(); ?>
                <div class="swiper-slide">
                    <div class="jumbotron oslide" style="background-image: url( <?php the_sub_field( 'baner_background' ); ?>">
                        <div class="baner--slide container nopadding">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1><?php the_sub_field( 'baner_title' ); ?></h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="ope"><?php the_sub_field( 'baner_description' ); ?><br/>
                                        <a class="btn btn-link nopadding" href="<?php the_sub_field( 'baner_link' ); ?>" role="button">Sprawdź...</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endwhile;
        endif; ?>
        </div>
    </div>
    <script>
        new Swiper ('#baner', {
            direction: 'horizontal',
            loop: true,
            autoplay: <?php the_field( 'slajder_speed_autoplay' ); ?>,
            speed: <?php the_field( 'slajder_speed' ); ?>,
            grabCursor: true,
            onSlideChangeStart: function(swiper){
                var animation = "<?php the_field( 'slajder_type_animation_content' ); ?>";
                var delay = "<?php the_field( 'slajder_speed_animation_content' ); ?>ms";
                jQuery('#baner .baner--slide').removeClass('animated').removeClass(animation).attr('');
                jQuery('#baner .swiper-slide-active .baner--slide').attr('style','-webkit-animation-delay:'+delay+';animation-delay:'+delay).addClass('animated').addClass(animation);
            }
        })
    </script>

		<?php if ( have_rows( 'products' ) ): ?>
            <div class="container ofer" style="padding-left: 50px; padding-right: 50px;">
                <div class="row">
					<?php while ( have_rows( 'products' ) ) : the_row(); ?>
						<?php $post = get_sub_field('products_simple_id'); ?>
						<?php $link = get_sub_field('products_simple_link'); ?>
						<?php setup_postdata($post); ?>
                        <a href="<?php echo $link; ?>" style="text-decoration: none;    color: #333;" class="col-sm-12 col-md-6 col-lg-3 text-center">
                            <div class="col-md-12">
                                <img src="<?php the_field('product_image'); ?>"/>
                            </div>
                            <h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
                        </a>
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endwhile; ?>
                </div>
            </div>
		<?php endif; ?>

        <div class="jumbotron pomoc" style="background-image: url(<?php the_field('point_background'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="duzy"><?php the_field('point_title'); ?></div>
                    </div>
                </div>

	            <?php $i=1; if ( have_rows( 'point' ) ):
                    while ( have_rows( 'point' ) ) : the_row(); ?>

                        <div class="col-sm-12 col-md-4 col-lg-2 text-center">
                            <div class="col-md-12">
                                <div class="kolko"><?php echo $i; $i++; ?></div>
                            </div>
                            <p><strong><?php the_sub_field( 'point_title' ); ?></strong></p>
                            <p><?php the_sub_field( 'point_description' ); ?></p>
                        </div>

		            <?php
	            endwhile;
	            endif; ?>
            </div>
        </div>

        <?php
		$post_objects = get_field('recenzje');
	    if( $post_objects ): ?>
        <div class="container zaufa">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="duzy"><?php the_field('recenzje_title'); ?></p>
                </div>
            </div>
            <div class="row">
            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <div class="col-md-6 mbtt">
                    <div class="col-md-1 cudzy">
                        "
                    </div>
                    <div class="col-md-11">
                        <i> <?php the_content(); ?></i>
                        <p class="name"><?php the_title(); ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif;  ?>

		<?php if ( have_rows( 'pictures' ) ): ?>
            <div class="row" style="margin-top: 50px; margin-bottom: 10px;">
            <?php while ( have_rows( 'pictures' ) ) : the_row(); ?>

                <a href="<?php the_sub_field( 'pictures_picture_link' ); ?>" class="col-xs-6 col-md-3 heipo trans rel nopadding" style="background-image: url(<?php the_sub_field( 'pictures_picture_img' ); ?>);">
                    <div class="centobg trans"><p class="cent trans"><?php the_sub_field( 'pictures_picture_title' ); ?></p></div>

                </a>

				<?php endwhile; ?>
            </div>
        <?php endif; ?>

        <div class="container poro">
            <!--<div class="row">
                <div class="col-md-12 text-center">
                    <p class="duzy"><?php the_field( 'fast_contact_title', 'option' ); ?></p>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-sm-12 col-md-4 braz">
			        <?php the_field( 'fast_contact_data', 'option' ); ?>
                    <img class="poroab" src="<?php the_field( 'fast_contact_background', 'option' ); ?>">
                </div>
                <div class="col-sm-12 col-md-8 poro">
                    <div id="map" class="map_fast"></div>
                </div>
		        <?php $map = get_field( 'fast_contact_map', 'option' );?>
                <script>
                    jQuery(document).ready(function() {
                        new GMaps({
                            el: '#map',
                            lat: <?php echo $map['lat']; ?>,
                            lng: <?php echo $map['lng']; ?>
                        });
                    });
                </script>
            </div>-->
            <div class="row">
                <div class="col-md-12 text-center nopadding">
                    <p><a class="btn btn-primary btn-huge" href="<?php the_field( 'fast_contact_link', 'option' ); ?>" role="button" style="margin-top: 25px;">Zamów bezpłatną wycenę</a></p>
                </div>
            </div>
        </div>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
