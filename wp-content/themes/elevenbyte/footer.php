<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
</div><!-- .site-content from header-->

</div><!-- .site-inner -->
</div><!-- .site -->



<footer>
    <div class="container nopadding">
        <hr>
    </div>
    <div class="container stopka">

        <div class="row">
            <div class="col-xs-1 dem">
                <img src="<?php the_field( 'dekoracja', 'option' ); ?>" />
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <p class="paragraph"><?php the_field( 'left_menu', 'option' ); ?></p>

	            <?php
	            wp_nav_menu( array(
			            'menu'              => 'primary',
			            'theme_location'    => 'primary',
			            'depth'             => 1,
			            'container'         => 'ul',
			            'menu_class'        => 'nav navbar-nav'
                    )
	            );
	            ?>

            </div>
            <div class="col-md-1">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-3 ona2">
                <p class="paragraph"><?php the_field( 'right_menu', 'option' ); ?></p>

	            <?php
	            wp_nav_menu( array(
			            'menu'              => 'secondary',
			            'theme_location'    => 'secondary',
			            'depth'             => 2,
			            'container'         => 'ul',
			            'menu_class'        => 'nav navbar-nav'
		            )
	            );
	            ?>

            </div>
            <div class="col-md-1">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <p class="ona paragraph"><?php the_field( 'footer_about_title', 'option' ); ?></p>
	            <?php the_field( 'footer_about_description', 'option' ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copy"><?php the_field( 'copyright', 'option' ); ?></p>
                </div>
            </div>
        </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
