<?php
/*
	Template Name: Realizacje
*/
get_header(); ?>

<?php
$args = array(
    'post_type' => 'realizations',
    'posts_per_page' => -1,
    'order'				=> 'ASC'
);
$realization_posts = new WP_Query($args);
$i = 0;
?>
<div class="container">
	<div class="row">
	<?php if ($realization_posts->have_posts()) : ?>
		<?php while ($realization_posts->have_posts()) :
            $realization_posts->the_post();
            $i++;
            if($i % 5 == 0){
                $i = 1;
            }

            $images_object = get_field('realization_gallery');
        ?>

			<?php if( $i == 1 || $i == 4) : ?>
				<div class="col-md-8">
					<div class="col-md-12 baps trans" data-images='<?php echo json_encode($images_object); ?>'  style="background-image: url(<?php the_field('realization_image'); ?>);">
						<p class="bebe"><?php echo the_title(); ?></p>
					</div>
				</div>
            <?php else : ?>
					<div class="col-md-4">
						<div data-post-id="<?php echo get_the_ID(); ?>" data-images='<?php echo json_encode($images_object); ?>' class="col-md-12 baps trans" style="background-image: url(<?php the_field('realization_image'); ?>);">
							<p class="bebe"><?php echo the_title(); ?></p>
						</div>
					</div>
			<?php endif; ?>
			
			<?php $counter++; ?>
		<?php
			endwhile;
            wp_reset_postdata();
        ?>
	<?php else: ?>
        <h2>Brak realizacji</h2>
    <?php endif; ?>
	</div>
</div>
<script type="text/javascript">
    ( function( $ ) {
        $('body').on('click','[data-images]', function( e ) {
            e.preventDefault();
            var $data = $(this).data('images');
            if($data){
                var images = [];
                $($data).each(function(key, value){
                    images.push({'href':value['url'],'title':value['title']});
                });
                $.swipebox(images,{
                  'hideBarsDelay': 0
                });
            }

        } );
    } )( jQuery );
</script>
<?php get_template_part( 'call-to-action', 'index' ); ?>
<?php get_footer(); ?>
