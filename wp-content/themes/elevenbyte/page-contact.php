<?php
/*
	Template Name: Kontakt
*/
get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) the_post(); { ?>

        <div class="container poro poroziel">
            <div class="row row-eq-height">
                <div class="col-sm-12 col-md-4 braz">
	                <?php the_field( 'fast_contact_data', 'option' ); ?>
                    <img class="poroab" src="<?php bloginfo('template_url')?>/images/telefek.jpg">
                </div>
                <div class="col-sm-12 col-md-8 poro" style="padding-right: 0;">
	                <?php the_field( 'contact-form' ); ?>
                </div>
            </div>
        </div>

        <div class="row konmap">
            <div class="col-sm-12">
                <div id="map"></div>
            </div>
        </div>
		<?php $map = get_field( 'fast_contact_map', 'option' );?>
        <script>
            jQuery(document).ready(function() {
                new GMaps({
                    el: '#map',
                    lat: <?php echo $map['lat']; ?>,
                    lng: <?php echo $map['lng']; ?>,
                });
            });
        </script>

		<?php
	}
} ?>




<?php get_footer(); ?>
