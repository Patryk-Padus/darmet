<?php
//CALL TO ACTION
?>
<div class="container poro">
    <div class="row">
        <div class="col-md-12 text-center">
            <p class="duzy"><?php the_field( 'fast_contact_title', 'option' ); ?></p>
		</div>
	</div>
	<div class="row row-eq-height">
		<div class="col-sm-12 col-md-4 braz">
			<?php the_field( 'fast_contact_data', 'option' ); ?>
			<img class="poroab" src="<?php the_field( 'fast_contact_background', 'option' ); ?>">
		</div>
		<div class="col-sm-12 col-md-8 poro">
			<div id="map" class="map_fast"></div>
		</div>
		<?php $map = get_field( 'fast_contact_map', 'option' );?>
		<script>
	        jQuery(document).ready(function() {
	            new GMaps({
	                el: '#map',
	                lat: <?php echo $map['lat']; ?>,
	                lng: <?php echo $map['lng']; ?>
	            });
	        });
		</script>
	</div>
	<div class="row">
		<div class="col-md-12 text-center nopadding">
			<p><a class="btn btn-primary btn-huge" href="<?php the_field( 'fast_contact_link', 'option' ); ?>" role="button" style="margin-top: 25px;">Zamów bezpłatną wycenę</a></p>
		</div>
	</div>
</div>
