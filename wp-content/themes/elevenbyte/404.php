<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header text-center">
					<h1 class="page-title"><?php the_field( 'error_404_title', 'option' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content text-center">
					<p><?php the_field( 'error_404_content', 'option' ); ?></p>
				</div>
			</section>

		</main>

	</div><!-- .content-area -->

<?php get_footer(); ?>
