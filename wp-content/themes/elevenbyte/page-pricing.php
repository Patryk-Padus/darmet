<?php
/*
	Template Name: Wycena
*/
get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) the_post(); { ?>

        <div class="container poro poroziel wycena">
            <div class="row ">
                <div class="col-md-12 text-center" style="margin-bottom: 35px;">
                    <p class="mba duzy"><?php the_title(); ?></p>
                    <p><?php the_content(); ?></p>
                </div>
                <div class="col-sm-8 poro" style="padding-right: 0; margin: 0 auto; float: none;">
	                <?php the_field( 'pircing-form' ); ?>
                </div>
            </div>
        </div>

		<?php
	}
} ?>

<?php get_footer(); ?>
