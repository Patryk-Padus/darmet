<?php
/*
	Template Name: Referencje
*/
get_header(); ?>

<?php
$args = array(
    'post_type' => 'reviews',
    'posts_per_page' => -1,
    'order'				=> 'ASC'
);
$reviews_posts = new WP_Query($args);
?>

<div class="container zaufa">
	<div class="row">
		<div class="col-md-12 text-center">
			<p class="duzy">Zaufali nam</p>
		</div>
	</div>
	<div class="row">
	<?php if ($reviews_posts->have_posts()) : ?>
		<?php while ($reviews_posts->have_posts()) :
            $reviews_posts->the_post();
        ?>
		<div class="col-md-6 mbtt">
			<div class="col-md-1 cudzy">
				"
			</div>
			<div class="col-md-11">
				<p><i>
						<?php the_content(); ?>
				</p></i>
				<p class="name"><?php echo the_title(); ?></p>
			</div>
		</div>
			
		<?php
			endwhile;
            wp_reset_postdata();
        ?>
		<?php else: ?>
			<h2>Brak referencji</h2>
		<?php endif; ?>
	</div>
</div>

<?php get_template_part( 'call-to-action', 'index' ); ?>
<?php get_footer(); ?>
